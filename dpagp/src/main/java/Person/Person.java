package Person;

public class Person {

    String firstname;
    String surname;
    double height;
    int year;
    String address;
    String job;
    int phoneNumber;

    //Methods (eat, drink, work)


    public Person(String firstname, String surname, double height, int year, String address, String job, int phoneNumber) {
        this.firstname = firstname;
        this.surname = surname;
        this.height = height;
        this.year = year;
        this.address = address;
        this.job = job;
        this.phoneNumber = phoneNumber;
    }

    public void eat() {
        printAction("eat");
    }

    public void drink() {
        printAction("drink");
    }

    public void work() {
        printAction("work");
    }

    private void printAction(String action){
        String message = String.format("My name is %s %s and I'm going to %s !!!!", firstname, surname, action);
        System.out.println(message);
    }

}
